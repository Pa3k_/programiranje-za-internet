<?php

    function connection(){
      $server ="localhost";
      $username = "root";
      $password = "";
      $database ="soundspread";

      $conn = mysqli_connect($server, $username, $password, $database);
      return $conn;
    }

    function createData($sql){
      $conn = connection();
      $res = mysqli_query($conn, $sql);
      mysqli_close($conn);   
    }

    function mysqlUpit($sql){
      $conn = connection();
      $rezultat = mysqli_query($conn, $sql);
      return $rezultat;
    }

?>